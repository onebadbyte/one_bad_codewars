# one_bad_codewars

Welcome to the **CodeWars Multi-Language Solutions** repository! This project showcases my solutions to various CodeWars challenges, solved in different programming languages including:

- **Go**
- **TypeScript**
- **Rust**
- **SQL**
- **Kotlin**

## About

This repository is a collection of my attempts to solve CodeWars problems while practicing and sharpening my skills in multiple programming languages. By tackling problems in different languages, I aim to:

- Enhance my problem-solving skills.
- Explore and understand the unique features of each language.
- Improve my fluency and confidence across multiple programming paradigms.
- Learn best practices for each language.

## Repository Structure

The repository is organized by language, and each language folder contains subfolders for specific problem categories or individual challenges:

```
├── go/
│   ├── main.go
│   ├── main_test.go
│   └── ...
├── typescript/
│   ├── problem-name-1.ts
│   ├── problem-name-2.ts
│   └── ...
├── rust/
│   ├── problem-name-1.rs
│   ├── problem-name-2.rs
│   └── ...
├── sql/
│   ├── problem-name-1.sql
│   ├── problem-name-2.sql
│   └── ...
├── kotlin/
│   ├── problem-name-1.kt
│   ├── problem-name-2.kt
│   └── ...
```

Each solution is named after the problem it solves and is accompanied by a brief description or comment header in the file explaining the approach taken.

## Getting Started

To explore and run the solutions, ensure you have the necessary tools installed for each language:

### Prerequisites

- **Go**: [Install Go](https://golang.org/doc/install)
- **TypeScript**: [Install Node.js and npm](https://nodejs.org/en/download/) and then install TypeScript via `npm install -g typescript`
- **Rust**: [Install Rust](https://www.rust-lang.org/tools/install)
- **SQL**: Use any SQL client or platform like MySQL, PostgreSQL, or SQLite to test solutions.
- **Kotlin**: [Install Kotlin](https://kotlinlang.org/docs/command-line.html)

### Running Solutions

Navigate to the respective language directory and execute the solutions using the appropriate commands:

#### Go
```bash
cd go
go run problem-name.go
```

#### TypeScript
```bash
cd typescript
ts-node problem-name.ts
```

#### Rust
```bash
cd rust
cargo run --bin problem-name
```

#### SQL
Run the `.sql` file in your preferred SQL client.

#### Kotlin
```bash
cd kotlin
kotlinc problem-name.kt -include-runtime -d problem-name.jar
java -jar problem-name.jar
```
## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

- Thanks to [CodeWars](https://www.codewars.com/) for providing an excellent platform for coding challenges.
- Thanks to the open-source community for creating and maintaining the tools and languages used in this project.

---



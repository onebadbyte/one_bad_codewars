package main_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	. "one_bad_server/one_bad_codewars/go"
)

var _ = Describe("Sample tests", func() {
	It("35231 becomes [1,3,2,5,3]", func() {
		Expect(Digitize(35231)).To(Equal([]int{1, 3, 2, 5, 3}))
	})

	It("0 becomes [0]", func() {
		Expect(Digitize(0)).To(Equal([]int{0}))
	})
})

var _ = Describe("DNA Test Example", func() {
	It("Basic Tests", func() {
		Expect(DNAStrand("AAAA")).To(Equal("TTTT"))
		Expect(DNAStrand("ATTGC")).To(Equal("TAACG"))
		Expect(DNAStrand("GTAT")).To(Equal("CATA"))
	})
})

var _ = Describe("Test Example", func() {
	It("just o", func() {
		Expect(ParseDeadFish("ooo")).To(Equal([]int{0, 0, 0}))
	})
	It("o&i", func() {
		Expect(ParseDeadFish("ioioio")).To(Equal([]int{1, 2, 3}))
	})
	It("o&i&d", func() {
		Expect(ParseDeadFish("idoiido")).To(Equal([]int{0, 1}))
	})
	It("o&i&d&s", func() {
		Expect(ParseDeadFish("isoisoiso")).To(Equal([]int{1, 4, 25}))
	})
	It("codewars", func() {
		Expect(ParseDeadFish("codewars")).To(Equal([]int{0}))
	})
})

var _ = Describe("Tests", func() {
	It("Sample tests", func() {
		Expect(MinMax([]int{1, 2, 3, 4, 5})).To(Equal([2]int{1, 5}))
		Expect(MinMax([]int{2334454, 5})).To(Equal([2]int{5, 2334454}))
		Expect(MinMax([]int{-2334454, -5})).To(Equal([2]int{-2334454, -5}))
		Expect(MinMax([]int{-5, -2334454})).To(Equal([2]int{-2334454, -5}))
	})

	It("Sample tests", func() {
		Expect(Between(1, 4)).To(Equal([]int{1, 2, 3, 4}))
		Expect(Between(-2, 2)).To(Equal([]int{-2, -1, 0, 1, 2}))
	})

	It("solution returns the correct value", func() {
		Expect(RomanNumerals(1)).To(Equal("I"))
		Expect(RomanNumerals(2)).To(Equal("II"))
		Expect(RomanNumerals(4)).To(Equal("IV"))
		Expect(RomanNumerals(5)).To(Equal("V"))
		Expect(RomanNumerals(9)).To(Equal("IX"))
		Expect(RomanNumerals(10)).To(Equal("X"))
		Expect(RomanNumerals(182)).To(Equal("CLXXXII"))
		Expect(RomanNumerals(500)).To(Equal("D"))
		Expect(RomanNumerals(1410)).To(Equal("MCDX"))
		Expect(RomanNumerals(1990)).To(Equal("MCMXC"))
		Expect(RomanNumerals(2008)).To(Equal("MMVIII"))
		Expect(RomanNumerals(2542)).To(Equal("MMDXLII"))
		Expect(RomanNumerals(1875)).To(Equal("MDCCCLXXV"))
	})

	It("'add' should return the two numbers added together!", func() {
		Expect(Arithmetic(8, 2, "add")).To(Equal(10))
	})
	It("'subtract' should return a minus b!", func() {
		Expect(Arithmetic(8, 2, "subtract")).To(Equal(6))
	})
	It("'multiply' should return a multiplied by b!", func() {
		Expect(Arithmetic(8, 2, "multiply")).To(Equal(16))
	})
	It("'divide' should return a divided by b!", func() {
		Expect(Arithmetic(8, 2, "divide")).To(Equal(4))
	})
})

var _ = Describe("String to array", func() {
	It("Sample tests", func() {
		Expect(StringToArray("Robin Singh")).To(Equal([]string{"Robin", "Singh"}))
		Expect(StringToArray("CodeWars")).To(Equal([]string{"CodeWars"}))
		Expect(StringToArray("I love arrays they are my favorite")).To(Equal([]string{"I", "love", "arrays", "they", "are", "my", "favorite"}))
		Expect(StringToArray("1 2 3")).To(Equal([]string{"1", "2", "3"}))
	})
})

var _ = Describe("Scorer Function", func() {
	It("should value this as worthless", func() {
		Expect(Score([5]int{2, 3, 4, 6, 2})).To(Equal(0), "Incorrect answer for dice=[2, 3, 4, 6, 2]")
	})

	It("should value this triplet correctly", func() {
		Expect(Score([5]int{4, 4, 4, 3, 3})).To(Equal(400), "Incorrect answer for dice=[4, 4, 4, 3, 3]")
	})

	It("should value this mixed set correctly", func() {
		Expect(Score([5]int{2, 4, 4, 5, 4})).To(Equal(450), "Incorrect answer for dice=[2, 4, 4, 5, 4]")
	})
	It("should value this straight set correctly", func() {
		Expect(Score([5]int{1, 2, 3, 4, 6})).To(Equal(100), "Incorrect answer for dice=[1, 2, 3, 4, 6]")
	})
	It("should value this straight 2 set correctly", func() {
		Expect(Score([5]int{1, 1, 2, 3, 6})).To(Equal(200), "Incorrect answer for dice=[1, 1, 2, 3, 6]")
	})
	It("should value this straight 3 set correctly", func() {
		Expect(Score([5]int{1, 1, 1, 3, 6})).To(Equal(1000), "Incorrect answer for edge 3")
	})
	It("should value this straight 4 set correctly", func() {
		Expect(Score([5]int{1, 1, 1, 1, 6})).To(Equal(1100), "Incorrect answer for edge 4")
	})
	It("should value this straight 5 set correctly", func() {
		Expect(Score([5]int{5, 5, 5, 3, 3})).To(Equal(500), "Incorrect answer for edge 5")
	})
	It("should value this straight 6 set correctly", func() {
		Expect(Score([5]int{2, 2, 2, 2, 3})).To(Equal(200), "Incorrect answer for edge 6")
	})
})

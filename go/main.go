package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println("testing")
}

func Digitize(n int) []int {
	var output []int
	i := strconv.Itoa(n)
	for _, number := range i {
		if i, err := strconv.Atoi(string(number)); err == nil {
			output = append([]int{i}, output...)
		}
	}
	// your code here
	return output
}

func DNAStrand(dna string) string {
	output := ""

	for _, val := range dna {
		switch string(val) {
		case "A":
			output += "T"
		case "C":
			output += "G"
		case "G":
			output += "C"
		case "T":
			output += "A"
		default:
			fmt.Errorf("Shouldn't ever be hit!")
		}
	}
	return output
}

func ParseDeadFish(data string) []int {
	output := make([]int, 0)
	current := 0
	for _, val := range data {
		switch string(val) {
		case "o":
			output = append(output, current)
		case "i":
			current += 1
		case "d":
			current -= 1
		case "s":
			current *= current
		default:
		}
	}

	return output
}

func MinMax(arr []int) [2]int {
	min := arr[0]
	max := arr[0]

	for _, x := range arr {
		if x < min {
			min = x
		} else if x > max {
			max = x
		}
	}

	if min > max {
		return [2]int{max, min}
	}

	return [2]int{min, max}
}

func Between(a, b int) []int {

	output := make([]int, 0)

	for x := a; x <= b; x++ {
		output = append(output, x)

	}

	return output
}

func RomanNumerals(number int) string {
	output := ""

	thou := number / 1000
	for x := 0; x < thou; x++ {
		output = output + "M"
		number -= 1000
	}

	if number/900 == 1 {
		output = output + "CM"
		number -= 900
	}

	fiveHundo := number / 500
	for x := 0; x < fiveHundo; x++ {
		output = output + "D"
		number -= 500
	}

	if number/400 == 1 {
		output = output + "CD"
		number -= 400
	}

	hundo := number / 100
	for x := 0; x < hundo; x++ {
		output = output + "C"
		number -= 100
	}

	if number/90 == 1 {
		output = output + "XC"
		number -= 90
	}

	fitty := number / 50
	for x := 0; x < fitty; x++ {
		output = output + "L"
		number -= 50
	}

	if number/40 == 1 {
		output = output + "XL"
		number -= 40

	}

	tens := number / 10
	for x := 0; x < tens; x++ {
		output = output + "X"
		number -= 10
	}

	if number/9 == 1 {
		output = output + "IX"
		number -= 9
	}

	fives := number / 5
	for x := 0; x < fives; x++ {
		output = output + "V"
		number -= 5
	}

	if number/4 == 1 {
		output = output + "IV"
		number -= 4
	}

	ones := number / 1
	for x := 0; x < ones; x++ {
		output = output + "I"
		number -= 1
	}

	return output
}

func Arithmetic(a int, b int, operator string) int {
	//your code here
	switch operator {

	case "add":
		return a + b
	case "subtract":
		return a - b
	case "multiply":
		return a * b
	case "divide":
		return a / b
	default:
		return 0

	}
}

func StringToArray(str string) []string {
	output := []string{}
	holder := ""
	for i, letter := range str {
		if letter == ' ' {
			output = append(output, holder)
			holder = ""
		} else {
			holder += string(letter)
		}

		if i == len(str)-1 {
			output = append(output, holder)
			return output
		}

	}
	return output
}

func Score(dice [5]int) int {
	score := 0
	index := map[int]int{
		1: 0,
		2: 0,
		3: 0,
		4: 0,
		5: 0,
		6: 0,
	}
	for _, x := range dice {
		index[x] += 1
	}

	for key, value := range index {
		if key == 1 || key == 5 {
			if key == 1 {
				if value >= 3 {
					score += (1000 + (value-3)*100)
				} else {
					score += (100 * value)
				}
			}
			if key == 5 {
				if value >= 3 {
					score += (500 + (value-3)*50)
				} else {
					score += (50 * value)
				}
			}
		} else {
			if value >= 3 {
				score += key * 100
			}
		}
	}

	return score
}

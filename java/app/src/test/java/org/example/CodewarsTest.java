
package org.example;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CodewarsTest {
  @Test
  void grassHopper() {
    assertEquals(true, Codewars.checkForFactor(10, 2));
    assertEquals(true, Codewars.checkForFactor(63, 7));
    assertEquals(true, Codewars.checkForFactor(2450, 5));
    assertEquals(true, Codewars.checkForFactor(24612, 3));
  }

  @Test
  public void setAlarmTest() {
    assertEquals(true, Codewars.setAlarm(true, false));
    assertEquals(false, Codewars.setAlarm(true, true));
    assertEquals(false, Codewars.setAlarm(false, false));
    assertEquals(false, Codewars.setAlarm(false, true));
  }
}

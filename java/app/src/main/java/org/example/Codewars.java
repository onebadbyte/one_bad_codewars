package org.example;

public class Codewars {

  public static boolean checkForFactor(int base, int factor) {
    return base % factor == 0;
  }

  public static boolean setAlarm(boolean employed, boolean vacation) {
    return employed && !vacation;
  }
}

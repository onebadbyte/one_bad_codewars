package main

import (
	"fmt"
)

func main() {
	fmt.Println("testing")
}

func sum(input []int) []int {
	output := []int{}
	if len(input) <= 0 {
		return output
	}

	total := 0

	for _, x := range input {
		total += x
		output = append(output, total)

	}
	return output
}

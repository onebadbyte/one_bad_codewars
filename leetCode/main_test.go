package main

import (
	"reflect"
	"testing"
)

func TestSum(t *testing.T) {

	input := []int{3, 1, 2, 10, 1}
	expected := []int{3, 4, 6, 16, 17}

	output := sum(input)

	if !reflect.DeepEqual(output, expected) {

		t.Errorf("Sum didn't work correctly got %v wanted %v", output, expected)
	}

}

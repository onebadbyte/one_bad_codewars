use std::fmt::format;

fn main() {
    println!("Hello, world!");
}

fn square_sum(mut arr: Vec<i8>) -> i8 {
    let mut total = 0;
    for elem in arr.iter_mut() {
        *elem *= *elem;
        total += *elem
    }
    total
}

#[test]
fn returns_expected() {
    assert_eq!(square_sum(vec![1, 2]), 5);
    assert_eq!(square_sum(vec![-1, -2]), 5);
    assert_eq!(square_sum(vec![5, 3, 4]), 50);
    assert_eq!(square_sum(vec![]), 0);
}

fn even_or_odd(number: i32) -> &'static str {
    if number % 2 == 0 {
        "Even"
    } else {
        "Odd"
    }
}

fn double_char(s: &str) -> String {
    let mut output = String::new();
    for x in s.chars() {
        output = format!("{}{}{}", output, x, x);
    }
    output
}

fn divisors(n: u32) -> u32 {
    let mut count = 0;
    for x in 1..n + 1 {
        if n % x == 0 {
            count += 1;
        }
    }
    count
}

fn sort_by_length(arr: &[String]) -> Vec<String> {
    let mut data: Vec<String> = Vec::from(arr);
    let mut output: Vec<String> = Vec::new();
    for x in data {
        println!("{}", x);
    }
    output
}

// Add your tests here.
// See https://doc.rust-lang.org/stable/rust-by-example/testing/unit_testing.html
#[cfg(test)]
mod tests_lenth {
    use super::sort_by_length;

    fn dotest(arr: &[String], expected: &[String]) {
        let actual = sort_by_length(arr);
        assert!(
            actual == expected,
            "With arr = {arr:?}\nExpected {expected:?} but got {actual:?}"
        )
    }

    #[test]
    fn fixed_tests() {
        dotest(
            &[
                String::from("beg"),
                String::from("life"),
                String::from("i"),
                String::from("to"),
            ],
            &[
                String::from("i"),
                String::from("to"),
                String::from("beg"),
                String::from("life"),
            ],
        );
        dotest(
            &[
                String::from(""),
                String::from("moderately"),
                String::from("brains"),
                String::from("pizza"),
            ],
            &[
                String::from(""),
                String::from("pizza"),
                String::from("brains"),
                String::from("moderately"),
            ],
        );
        dotest(
            &[
                String::from("longer"),
                String::from("longest"),
                String::from("short"),
            ],
            &[
                String::from("short"),
                String::from("longer"),
                String::from("longest"),
            ],
        );
        dotest(
            &[
                String::from("dog"),
                String::from("food"),
                String::from("a"),
                String::from("of"),
            ],
            &[
                String::from("a"),
                String::from("of"),
                String::from("dog"),
                String::from("food"),
            ],
        );
        dotest(
            &[
                String::from(""),
                String::from("dictionary"),
                String::from("eloquent"),
                String::from("bees"),
            ],
            &[
                String::from(""),
                String::from("bees"),
                String::from("eloquent"),
                String::from("dictionary"),
            ],
        );
        dotest(
            &[
                String::from("a longer sentence"),
                String::from("the longest sentence"),
                String::from("a short sentence"),
            ],
            &[
                String::from("a short sentence"),
                String::from("a longer sentence"),
                String::from("the longest sentence"),
            ],
        );
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    #[test]
    fn sample_tests() {
        assert_eq!(divisors(1), 1);
        assert_eq!(divisors(4), 3);
        assert_eq!(divisors(5), 2);
        assert_eq!(divisors(12), 6);
        assert_eq!(divisors(25), 3);
        assert_eq!(divisors(4096), 13);
    }

    fn dotest(s: &str, expected: &str) {
        let actual = double_char(s);
        assert!(
            actual == expected,
            "With s = \"{s}\"\nExpected \"{expected}\" but got \"{actual}\""
        )
    }

    #[test]
    fn test_hello_world() {
        dotest("Hello World", "HHeelllloo  WWoorrlldd")
    }
    #[test]
    fn test_numbers() {
        dotest("1234!_ ", "11223344!!__  ")
    }

    fn do_test(number: i32, expected: &str) {
        let actual = even_or_odd(number);
        assert_eq!(actual, expected, "\nYour result (left) does not match the expected output (right) for the input {number:?}");
    }

    #[test]
    fn test_zero() {
        do_test(0, "Even");
    }

    #[test]
    fn test_positive_even() {
        do_test(2, "Even");
    }

    #[test]
    fn test_positive_odd() {
        do_test(1, "Odd");
    }

    #[test]
    fn test_negative_even() {
        do_test(-2, "Even");
    }

    #[test]
    fn test_negative_odd() {
        do_test(-1, "Odd");
    }
}

fn is_leap_year(year: i32) -> bool {
    year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)
}

// Add your tests here.
// See https://doc.rust-lang.org/stable/rust-by-example/testing/unit_testing.html

#[cfg(test)]
mod sample_tests1 {
    use super::is_leap_year;

    fn do_test(year: i32, expected: bool) {
        let actual = is_leap_year(year);
        assert_eq!(
            actual, expected,
            "\nYour result (left) does not match the expected output (right) for the year {year:?}"
        );
    }

    #[test]
    fn year_2020_is_a_leap_year() {
        do_test(2020, true);
    }

    #[test]
    fn year_2000_is_a_leap_year() {
        do_test(2000, true);
    }

    #[test]
    fn year_2015_is_not_a_leap_year() {
        do_test(2015, false);
    }

    #[test]
    fn year_2100_is_not_a_leap_year() {
        do_test(2100, false);
    }
}

fn solution(s: &str) -> String {
    let check_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let output = s.chars().map(|x| {
        if check_chars.contains(x) {
            format!(" {}", x)
        } else {
            x.to_string()
        }
    });
    output.collect()
}

// Add your tests here.
// See https://doc.rust-lang.org/stable/rust-by-example/testing/unit_testing.html

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_camel_solution() {
        assert_eq!(solution("camelCasing"), "camel Casing");
        assert_eq!(solution("camelCasingTest"), "camel Casing Test");
    }
}

fn multiplication_table(len: usize) -> Vec<Vec<usize>> {
    let mut output: Vec<Vec<usize>> = Vec::new();
    for x in 1..=len {
        let mut arr_len: Vec<usize> = Vec::from_iter(1..=len);
        for y in arr_len.iter_mut() {
            *y *= x
        }
        output.push(arr_len);
    }
    output
}

// Add your tests here.
// See https://doc.rust-lang.org/stable/rust-by-example/testing/unit_testing.html

#[cfg(test)]
mod multiplication_table_tests {
    use super::*;

    #[test]
    fn basic() {
        assert_eq!(multiplication_table(3), [[1, 2, 3], [2, 4, 6], [3, 6, 9]]);
    }
}
